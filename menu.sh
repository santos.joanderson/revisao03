#!/bin/bash


while [ "$op" != "5" ]; do

	echo "1 - Digite o nome de um arquivo"
	echo "2 - Criar um arquivo"
	echo "3 - Adicionar um número ao final do arquivo"
	echo "4 - Remove o arquivo"
	echo "5 - Sair"
	read -p "Escolha uma das opções: " op

	case $op in
		1)read -p "Digite o nome do arquivo: " teste;;
		2)touch $teste && echo "O arquivo foi criado com sucesso $teste";;
		3)read -p "Digite o numero que deseja adicionar ao final: " numero >> $teste && echo "Foi adicionado $numero";;
		4)rm $teste;;
		5)exit 0;;
	esac
done
